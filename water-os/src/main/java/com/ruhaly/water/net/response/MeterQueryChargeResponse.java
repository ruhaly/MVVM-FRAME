package com.ruhaly.water.net.response;

import com.ruhaly.common_lib.net.response.BaseResponse;
import com.ruhaly.water.model.MeterQueryResult;

import java.util.List;

/**
 * Created by han_l on 2017-01-18.
 */
public class MeterQueryChargeResponse extends BaseResponse<MeterQueryChargeResponse> {

    List<MeterQueryResult> rows;

    String rowCount;

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public List<MeterQueryResult> getRows() {
        return rows;
    }

    public void setRows(List<MeterQueryResult> rows) {
        this.rows = rows;
    }
}
