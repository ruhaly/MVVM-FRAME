package com.ruhaly.water.net;

/**
 * Created by han_l on 2016/10/24.
 */
public class Urls {
    public static final String URL_LOGIN = ApiConstants.BASE_URL + "/account/login";
    public static final String URL_LOGOUT = ApiConstants.BASE_URL + "/account/logout";
}
