package com.ruhaly.water.net;

/**
 *
 */
public class ApiConstants {
    public static final String BASE_URL = "http://mail.luichi.info:8885/appserver";
    public static final String LOGIN_URL = "/mobile/login.htm";
    public static final String QUERY_URL = "/mobile/quickread/query.htm";
    public static final String WRITE_URL = "/mobile/quickread/write.htm";

    public static final String QUERY_CANCEL_URL = "/mobile/cancelread/query.htm";
    public static final String WRITE_CANCEL_URL = "/mobile/cancelread/write.htm";
    public static final String QUERY_QUICK_CHARGE_URL = "/mobile/quickcharge/query.htm";
    public static final String WRITE_QUICK_CHARGE_URL = "/mobile/quickcharge/write.htm";

    public static final String QUERY_CANCEL_CHARGE_URL = "/mobile/cancelcharge/query.htm";
    public static final String WRITE_CANCEL_CHARGE_URL = "/mobile/cancelcharge/write.htm";

    public static final String QUERY_READ_METER_URL = "/mobile/queryread.htm";
    public static final String QUERY_HAS_CHARGE_URL = "/mobile/querycharge.htm";
}
