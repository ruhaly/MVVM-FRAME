package com.ruhaly.water.net;

import android.graphics.Bitmap;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.convert.BitmapConvert;
import com.lzy.okgo.convert.FileConvert;
import com.lzy.okgo.convert.StringConvert;
import com.lzy.okrx.RxAdapter;
import com.ruhaly.common_lib.callback.JsonConvert;
import com.ruhaly.common_lib.net.response.BaseResponse;
import com.ruhaly.common_lib.utils.JsonUtil;
import com.ruhaly.water.common.Constant;
import com.ruhaly.water.model.MeterQueryCondition;
import com.ruhaly.water.model.MeterQueryResult;
import com.ruhaly.water.model.User;
import com.ruhaly.water.net.response.LoginResponse;
import com.ruhaly.water.net.response.MeterQueryChargeResponse;
import com.ruhaly.water.net.response.MeterQueryResponse;

import java.io.File;
import java.util.List;

import rx.Observable;

import static com.ruhaly.water.net.ApiConstants.LOGIN_URL;
import static com.ruhaly.water.net.ApiConstants.QUERY_CANCEL_CHARGE_URL;
import static com.ruhaly.water.net.ApiConstants.QUERY_CANCEL_URL;
import static com.ruhaly.water.net.ApiConstants.QUERY_HAS_CHARGE_URL;
import static com.ruhaly.water.net.ApiConstants.QUERY_QUICK_CHARGE_URL;
import static com.ruhaly.water.net.ApiConstants.QUERY_READ_METER_URL;
import static com.ruhaly.water.net.ApiConstants.QUERY_URL;
import static com.ruhaly.water.net.ApiConstants.WRITE_CANCEL_CHARGE_URL;
import static com.ruhaly.water.net.ApiConstants.WRITE_CANCEL_URL;
import static com.ruhaly.water.net.ApiConstants.WRITE_QUICK_CHARGE_URL;
import static com.ruhaly.water.net.ApiConstants.WRITE_URL;

public class ApiService {

    public static Observable<BaseResponse<LoginResponse>> login(User user) {
        return OkGo.post(Constant.getServerUrl().concat(LOGIN_URL))
                .params("username", user.getUserName())
                .params("password", user.getPwd())
                .params("telephone", user.getPhone())
                .getCall(new JsonConvert<BaseResponse<LoginResponse>>() {
                }, RxAdapter.<BaseResponse<LoginResponse>>create());
    }

    public static Observable<BaseResponse<MeterQueryResponse>> meterQuery(MeterQueryCondition meterQueryCondition) {
        return OkGo.post(Constant.getServerUrl().concat(QUERY_URL))
                .params(JsonUtil.jsonToMap(JsonUtil.toJson(meterQueryCondition)))
                .getCall(new JsonConvert<BaseResponse<MeterQueryResponse>>() {
                }, RxAdapter.<BaseResponse<MeterQueryResponse>>create());
    }

    public static Observable<BaseResponse<String>> saveMeterInfo(MeterQueryResult meterQueryResult) {
        return OkGo.post(Constant.getServerUrl().concat(WRITE_URL))
                .params("userid", meterQueryResult.getUserid())
                .params("usercode", meterQueryResult.getUsercode())
                .params("readmonth", meterQueryResult.getReadmonth())
                .params("lastreading", meterQueryResult.getLastreading())
                .params("currentreading", meterQueryResult.getCurrentreading())
                .params("lastwateramount", meterQueryResult.getLastwateramount())
                .params("employeeid", meterQueryResult.getEmployeeid())
                .getCall(new JsonConvert<BaseResponse<String>>() {
                }, RxAdapter.<BaseResponse<String>>create());
    }

    public static Observable<BaseResponse<String>> fastCharge(MeterQueryResult meterQueryResult) {
        return OkGo.post(Constant.getServerUrl().concat(WRITE_QUICK_CHARGE_URL))
                .params("userwaterinfoid", meterQueryResult.getUserwaterinfoid())
                .params("userid", meterQueryResult.getUserid())
                .params("readmonth", meterQueryResult.getReadmonth())
                .params("employeeid", meterQueryResult.getEmployeeid())
                .getCall(new JsonConvert<BaseResponse<String>>() {
                }, RxAdapter.<BaseResponse<String>>create());
    }

    public static Observable<BaseResponse<MeterQueryChargeResponse>> fastChargeQuery(MeterQueryCondition meterQueryCondition) {
        return OkGo.post(Constant.getServerUrl().concat(QUERY_QUICK_CHARGE_URL))
                .params("usercode", meterQueryCondition.getUsercode())
                .params("employeeid", meterQueryCondition.getEmployeeid())
                .getCall(new JsonConvert<BaseResponse<MeterQueryChargeResponse>>() {
                }, RxAdapter.<BaseResponse<MeterQueryChargeResponse>>create());
    }

    public static Observable<BaseResponse<MeterQueryChargeResponse>> chargeCancelQuery(MeterQueryCondition meterQueryCondition) {
        return OkGo.post(Constant.getServerUrl().concat(QUERY_CANCEL_CHARGE_URL))
                .params("usercode", meterQueryCondition.getUsercode())
                .params("readmonth", meterQueryCondition.getReadmonth())
                .params("chargedate", meterQueryCondition.getChargedate())
                .params("employeeid", meterQueryCondition.getEmployeeid())
                .getCall(new JsonConvert<BaseResponse<MeterQueryChargeResponse>>() {
                }, RxAdapter.<BaseResponse<MeterQueryChargeResponse>>create());
    }

    public static Observable<BaseResponse<MeterQueryChargeResponse>> hasReadMeterQuery(MeterQueryCondition meterQueryCondition) {
        return OkGo.post(Constant.getServerUrl().concat(QUERY_READ_METER_URL))
                .params("usercode", meterQueryCondition.getUsercode())
                .params("readmonth", meterQueryCondition.getReadmonth())
                .params("bookid", meterQueryCondition.getBookid())
                .params("regionid", meterQueryCondition.getRegionid())
                .params("pageNum", meterQueryCondition.getPageNum())
                .params("employeeid", meterQueryCondition.getEmployeeid())
                .getCall(new JsonConvert<BaseResponse<MeterQueryChargeResponse>>() {
                }, RxAdapter.<BaseResponse<MeterQueryChargeResponse>>create());
    }

    public static Observable<BaseResponse<MeterQueryChargeResponse>> hasChargeQuery(MeterQueryCondition meterQueryCondition) {
        return OkGo.post(Constant.getServerUrl().concat(QUERY_HAS_CHARGE_URL))
                .params("usercode", meterQueryCondition.getUsercode())
                .params("readmonth", meterQueryCondition.getReadmonth())
                .params("chargedate", meterQueryCondition.getChargedate())
                .params("employeeid", meterQueryCondition.getEmployeeid())
                .params("pageNum", meterQueryCondition.getPageNum())
                .getCall(new JsonConvert<BaseResponse<MeterQueryChargeResponse>>() {
                }, RxAdapter.<BaseResponse<MeterQueryChargeResponse>>create());
    }


    public static Observable<BaseResponse<String>> chargeCancel(MeterQueryResult meterQueryResult) {
        return OkGo.post(Constant.getServerUrl().concat(WRITE_CANCEL_CHARGE_URL))
                .params("userwaterinfoid", meterQueryResult.getUserwaterinfoid())
                .params("userid", meterQueryResult.getUserid())
                .params("readmonth", meterQueryResult.getReadmonth())
                .params("chargedate", meterQueryResult.getChargedate())
                .params("employeeid", meterQueryResult.getEmployeeid())
                .getCall(new JsonConvert<BaseResponse<String>>() {
                }, RxAdapter.<BaseResponse<String>>create());
    }

    public static Observable<BaseResponse<MeterQueryResponse>> meterCancelQuery(MeterQueryCondition meterQueryCondition) {
        return OkGo.post(Constant.getServerUrl().concat(QUERY_CANCEL_URL))
                .params(JsonUtil.jsonToMap(JsonUtil.toJson(meterQueryCondition)))
                .getCall(new JsonConvert<BaseResponse<MeterQueryResponse>>() {
                }, RxAdapter.<BaseResponse<MeterQueryResponse>>create());
    }

    public static Observable<BaseResponse<String>> saveCancelMeterInfo(MeterQueryResult meterQueryResult) {
        return OkGo.post(Constant.getServerUrl().concat(WRITE_CANCEL_URL))
                .params("userwaterinfoid", meterQueryResult.getUserwaterinfoid())
                .params("userid", meterQueryResult.getUserid())
                .params("readmonth", meterQueryResult.getReadmonth())
                .params("employeeid", meterQueryResult.getEmployeeid())
                .getCall(new JsonConvert<BaseResponse<String>>() {
                }, RxAdapter.<BaseResponse<String>>create());
    }

    public static Observable<String> logout() {
        return OkGo.post(Urls.URL_LOGOUT)//
                .getCall(StringConvert.create(), RxAdapter.<String>create());
    }

    public static Observable<BaseResponse<List<User>>> getServerListModel(String header, String param) {
        return OkGo.post("")//
                .headers("aaa", header)//
                .params("bbb", param)//
                .getCall(new JsonConvert<BaseResponse<List<User>>>() {
                }, RxAdapter.<BaseResponse<List<User>>>create());
    }

    public static Observable<Bitmap> getBitmap(String header, String param) {
        return OkGo.post("")//
                .headers("aaa", header)//
                .params("bbb", param)//
                .getCall(BitmapConvert.create(), RxAdapter.<Bitmap>create());
    }

    public static Observable<File> getFile(String header, String param) {
        return OkGo.post("")//
                .headers("aaa", header)//
                .params("bbb", param)//
                .getCall(new FileConvert(), RxAdapter.<File>create());
    }
}
