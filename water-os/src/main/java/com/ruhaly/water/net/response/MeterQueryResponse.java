package com.ruhaly.water.net.response;

import com.ruhaly.common_lib.net.response.BaseResponse;
import com.ruhaly.water.model.MeterQueryResult;

/**
 * Created by han_l on 2017-01-18.
 */
public class MeterQueryResponse extends BaseResponse<MeterQueryResponse> {
    MeterQueryResult book;

    public MeterQueryResult getBook() {
        return book;
    }

    public void setBook(MeterQueryResult book) {
        this.book = book;
    }
}
