package com.ruhaly.water.net.response;

import com.ruhaly.common_lib.net.response.BaseResponse;
import com.ruhaly.water.model.Menu;

import java.util.List;

/**
 * Created by han_l on 2016/9/13.
 */

public class LoginResponse extends BaseResponse<LoginResponse> {

    String employeeid;

    List<Menu> menus;

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
}
