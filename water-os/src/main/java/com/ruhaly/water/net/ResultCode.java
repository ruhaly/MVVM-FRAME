package com.ruhaly.water.net;

/**
 * Created by han_l on 2016/9/22.
 */

public interface ResultCode {

    String CODE_RESULT_SUCCESS = "2000";

    String CODE_LOGIN_INVALID = "5001";

    String CODE_UN_LOGIN_INVALID = "5000";

    String CODE_SERVER_ERROR = "500";

    String CODE_NET_ERROR = "0";

    String CODE_DATA_ERROR = "3000";


}
