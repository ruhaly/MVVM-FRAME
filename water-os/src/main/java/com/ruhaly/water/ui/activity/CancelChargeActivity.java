package com.ruhaly.water.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.jude.easyrecyclerview.decoration.DividerDecoration;
import com.ruhaly.water.R;
import com.ruhaly.water.databinding.ActivityCancelChargeBinding;
import com.ruhaly.water.manager.LogicManager;
import com.ruhaly.water.model.MeterQueryResult;
import com.ruhaly.common_lib.base.BaseActivity;
import com.ruhaly.common_lib.utils.Utils;
import com.ruhaly.water.viewmodel.CancelChargeViewModel;


public class CancelChargeActivity extends BaseActivity<ActivityCancelChargeBinding> {
    CancelChargeViewModel viewModel;
    String employeeId;
    Adapter adapter;

    @Override
    public void initTitleFrame() {
        getB().titleFrame.tvTitle.setText("取消收费");
        getB().titleFrame.tvRight.setText("查询");
        getB().titleFrame.tvRight.setVisibility(View.VISIBLE);
        getB().titleFrame.tvRight.setOnClickListener(view -> queryMeterInfo(true));
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_cancel_charge;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        initVM();
       /*以下控件设置*/
    }

    public void initVM() {
        viewModel = initVM(CancelChargeViewModel.class);
        getB().setViewModel(viewModel);

        employeeId = getIntent().getStringExtra("EMPLOYEE_ID");

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        getB().recyclerView.setLayoutManager(gridLayoutManager);
        getB().recyclerView.addItemDecoration(new DividerDecoration(getResources().getColor(R.color.white), 4));
        adapter = new Adapter(getBaseContext());
        getB().recyclerView.setAdapter(adapter);
        adapter.setMore(R.layout.view_more, new RecyclerArrayAdapter.OnMoreListener() {
            @Override
            public void onMoreShow() {
                queryMeterInfo(false);
            }

            @Override
            public void onMoreClick() {

            }
        });
        adapter.setError(R.layout.view_error, new RecyclerArrayAdapter.OnErrorListener() {
            @Override
            public void onErrorShow() {

            }

            @Override
            public void onErrorClick() {
                adapter.resumeMore();
            }
        });
        adapter.setNoMore(R.layout.view_nomore);
    }

    class Adapter extends RecyclerArrayAdapter<MeterQueryResult> {

        public Adapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(parent, R.layout.item_meter_query_charge_result);
        }
    }

    class ViewHolder extends BaseViewHolder<MeterQueryResult> {
        TextInputEditText userCode;
        TextInputEditText userName;
        TextInputEditText userAddr;
        TextInputEditText readMonth;
        TextInputEditText lastReading;
        TextInputEditText currentReading;
        TextInputEditText waterAmount;
        TextInputEditText adjustedAmount;
        TextView tvTitle;
        TextView btnCharge;
        LinearLayout frameTitle;
        LinearLayout frameChild;


        public ViewHolder(ViewGroup parent, @LayoutRes int res) {
            super(parent, res);
            frameTitle = $(R.id.frame_title);
            userCode = $(R.id.et_user_code_res);
            userName = $(R.id.et_user_name_res);
            userAddr = $(R.id.et_book_user_addr_res);
            readMonth = $(R.id.et_read_month_res);
            lastReading = $(R.id.et_last_reading_res);
            currentReading = $(R.id.et_current_reading_res);
            waterAmount = $(R.id.et_water_amount_res);
            adjustedAmount = $(R.id.et_adjusted_amount_res);
            tvTitle = $(R.id.tv_title);
            btnCharge = $(R.id.btn_charge);
            frameTitle = $(R.id.frame_title);
            frameChild = $(R.id.frame_child);
            btnCharge.setText("取消");
            btnCharge.setOnClickListener(view -> chargeCancel(getAdapterPosition()));
            frameTitle.setOnClickListener(view -> frameChild.setVisibility(frameChild.getVisibility() == View.GONE ? View.VISIBLE : View.GONE));

        }

        @Override
        public void setData(MeterQueryResult data) {
            tvTitle.setText(data.getReadmonth() + "/￥" + data.getAdjustedamount());
            userCode.setText(data.getUsercode());
            userName.setText(data.getUsername());
            userAddr.setText(data.getUseraddr());
            readMonth.setText(data.getReadmonth());
            lastReading.setText(data.getLastreading());
            currentReading.setText(data.getCurrentreading());
            waterAmount.setText(data.getWateramount());
            adjustedAmount.setText(data.getAdjustedamount());
            if (getAdapterPosition() == 0) {
                frameChild.setVisibility(View.VISIBLE);
            } else {
                frameChild.setVisibility(View.GONE);
            }
        }
    }

    int rowCount = 0;

    public void queryMeterInfo(boolean isFresh) {
        viewModel.getMeterQueryCondition().setEmployeeid(employeeId);
        LogicManager.getInstance().chargeCancelQuery(viewModel.getMeterQueryCondition())
                .compose(bindViewLifecycleTransformer(true))
                .subscribe(meterQueryChargeResponseBaseResponse -> {
                    if (isFresh) {
                        adapter.clear();
                    }
                    adapter.addAll(meterQueryChargeResponseBaseResponse.getData().getRows());
                    adapter.pauseMore();
                    if (adapter.getCount() >= rowCount) {
                        adapter.stopMore();
                    }
                    showProgress(false);
                }, throwable -> {
                    adapter.pauseMore();
                    showProgress(false);
                    handleThrowable(throwable);
                });
    }

    public void chargeCancel(int position) {
        viewModel.setMeterQueryResult(adapter.getItem(position));
        viewModel.getMeterQueryResult().setEmployeeid(employeeId);
        LogicManager.getInstance().chargeCancel(viewModel.getMeterQueryResult())
                .compose(bindViewLifecycleTransformer(true))
                .subscribe(meterQueryResponseBaseResponse -> {
                    Utils.snack(this, "取消收费成功");
                    showProgress(false);
                    viewModel.notifyChange();
                    queryMeterInfo(true);
                }, throwable -> {
                    showProgress(false);
                    handleThrowable(throwable);
                });
    }


}
