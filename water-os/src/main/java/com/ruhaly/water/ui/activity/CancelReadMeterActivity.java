package com.ruhaly.water.ui.activity;

import android.os.Bundle;
import android.view.View;

import com.ruhaly.water.R;
import com.ruhaly.water.databinding.ActivityCancelReadMeterBinding;
import com.ruhaly.water.manager.LogicManager;
import com.ruhaly.common_lib.base.BaseActivity;
import com.ruhaly.common_lib.utils.Utils;
import com.ruhaly.water.viewmodel.CancelReadMeterViewModel;


public class CancelReadMeterActivity extends BaseActivity<ActivityCancelReadMeterBinding> {
    CancelReadMeterViewModel viewModel;

    String employeeId;

    @Override
    public void initTitleFrame() {
        getB().titleFrame.tvTitle.setText("取消抄表");
        getB().titleFrame.tvRight.setVisibility(View.VISIBLE);
        getB().titleFrame.tvRight.setOnClickListener(view -> queryMeterInfo());
        getB().btnSave.setText("取消抄表");
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_cancel_read_meter;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        initVM();
       /*以下控件设置*/
    }

    public void initVM() {
        viewModel = initVM(CancelReadMeterViewModel.class);
        getB().setViewModel(viewModel);
        getB().meterQueryCondition.inputLayoutUserName.setVisibility(View.GONE);
        getB().meterQueryCondition.inputLayoutMobilePhone.setVisibility(View.GONE);
        getB().meterQueryCondition.inputLayoutRegionId.setVisibility(View.GONE);
        getB().meterQueryCondition.inputLayoutTelephone.setVisibility(View.GONE);
        getB().meterQueryCondition.inputLayoutBookId.setVisibility(View.GONE);


        employeeId = getIntent().getStringExtra("EMPLOYEE_ID");
        viewModel.getMeterQueryCondition().setEmployeeid(employeeId);

    }

    public void queryMeterInfo() {
        LogicManager.getInstance().meterCancelQuery(viewModel.getMeterQueryCondition())
                .compose(bindViewLifecycleTransformer(true))
                .subscribe(meterQueryResponseBaseResponse -> {
                    viewModel.setMeterQueryResult(meterQueryResponseBaseResponse.getData().getBook());
                    viewModel.notifyChange();
                    showProgress(false);
                }, throwable -> {
                    showProgress(false);
                    handleThrowable(throwable);
                });
    }

    public void saveMeterInfo(View view) {
        viewModel.getMeterQueryResult().setEmployeeid(employeeId);
        LogicManager.getInstance().saveCancelMeterInfo(viewModel.getMeterQueryResult())
                .compose(bindViewLifecycleTransformer(true))
                .subscribe(meterQueryResponseBaseResponse -> {
                    Utils.snack(this, "取消保存成功");
                    showProgress(false);
                    viewModel.setMeterQueryResult(null);
                    viewModel.notifyChange();
                    queryMeterInfo();
                }, throwable -> {
                    showProgress(false);
                    handleThrowable(throwable);
                });
    }
}
