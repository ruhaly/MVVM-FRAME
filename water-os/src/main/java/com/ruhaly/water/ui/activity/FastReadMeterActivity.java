package com.ruhaly.water.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.ruhaly.water.R;
import com.ruhaly.water.databinding.ActivityFastReadMeterBinding;
import com.ruhaly.water.manager.LogicManager;
import com.ruhaly.water.model.MeterQueryCondition;
import com.ruhaly.water.model.MeterQueryResult;
import com.ruhaly.common_lib.base.BaseActivity;
import com.ruhaly.common_lib.utils.JsonUtil;
import com.ruhaly.common_lib.utils.Utils;
import com.ruhaly.water.viewmodel.FastReadMeterViewModel;

/**
 * 快速抄表界面
 */
public class FastReadMeterActivity extends BaseActivity<ActivityFastReadMeterBinding> {
    FastReadMeterViewModel viewModel;

    String employeeId;

    @Override
    public void initTitleFrame() {
        getB().titleFrame.tvTitle.setText("快速抄表");
        getB().titleFrame.tvRight.setText("查询");
        getB().titleFrame.tvRight.setVisibility(View.VISIBLE);
        getB().titleFrame.tvRight.setOnClickListener(view -> queryMeterInfo(true, "1"));
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_fast_read_meter;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        initVM();
       /*以下控件设置*/
    }

    public void initVM() {
        viewModel = initVM(FastReadMeterViewModel.class);
        getB().setViewModel(viewModel);
        employeeId = getIntent().getStringExtra("EMPLOYEE_ID");
        viewModel.getMeterQueryCondition().setEmployeeid(employeeId);
        getB().meterQueryResult.etCurrentReadingRes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString()) || Double.valueOf(s.toString()) < Double.valueOf(viewModel.getMeterQueryResult().getLastreading())) {
                    getB().meterQueryResult.inputLayoutCurrentReadingRes.setErrorEnabled(true);
                    getB().meterQueryResult.inputLayoutCurrentReadingRes.setError("本次读数必须>=上次读数");
                    getB().meterQueryResult.etCurrentReadingRes.requestFocus();
                } else {
                    getB().meterQueryResult.inputLayoutCurrentReadingRes.setErrorEnabled(false);
                    Double d = Double.valueOf(s.toString()) - Double.valueOf(viewModel.getMeterQueryResult().getLastreading());
                    viewModel.getMeterQueryResult().setWateramount(d + "");
                }

            }
        });
    }

    public void queryMeterInfo(boolean isRefresh, String pageNum) {
        if (isRefresh) {
            viewModel.getMeterQueryCondition().setPageNum("1");
        } else {
            viewModel.getMeterQueryConditionMore().setPageNum(pageNum);
        }
        viewModel.setMeterQueryResult(new MeterQueryResult());
        viewModel.notifyChange();
        if (isRefresh) {
            LogicManager.getInstance().meterQuery(viewModel.getMeterQueryCondition())
                    .compose(bindViewLifecycleTransformer(true))
                    .subscribe(meterQueryResponseBaseResponse -> {
                        viewModel.setMeterQueryConditionMore(JsonUtil.fromJson(JsonUtil.toJson(viewModel.getMeterQueryCondition()), MeterQueryCondition.class));
                        viewModel.setMeterQueryResult(meterQueryResponseBaseResponse.getData().getBook());
                        viewModel.notifyChange();
                        showProgress(false);
                    }, throwable -> {
                        showProgress(false);
                        handleThrowable(throwable);
                    });
        } else {
            LogicManager.getInstance().meterQuery(viewModel.getMeterQueryConditionMore())
                    .compose(bindViewLifecycleTransformer(true))
                    .subscribe(meterQueryResponseBaseResponse -> {
                        viewModel.setMeterQueryResult(meterQueryResponseBaseResponse.getData().getBook());
                        viewModel.notifyChange();
                        showProgress(false);
                    }, throwable -> {
                        showProgress(false);
                        handleThrowable(throwable);
                    });
        }
    }

    public void saveMeterInfo(View view) {
        if (getB().meterQueryResult.inputLayoutLastReadingRes.isErrorEnabled()) {
            return;
        }
        viewModel.getMeterQueryResult().setEmployeeid(employeeId);
        LogicManager.getInstance().saveMeterInfo(viewModel.getMeterQueryResult())
                .compose(bindViewLifecycleTransformer(true))
                .subscribe(meterQueryResponseBaseResponse -> {
                    Utils.snack(this, "保存成功");
                    showProgress(false);
                    viewModel.setMeterQueryResult(null);
                    viewModel.notifyChange();
                    queryMeterInfo(true, "1");
                }, throwable -> {
                    showProgress(false);
                    handleThrowable(throwable);
                });
    }


    public void lastQuery(View view) {

        if (TextUtils.isEmpty(viewModel.getMeterQueryResult().getPageNum()) || Integer.valueOf(viewModel.getMeterQueryResult().getPageNum()) <= 1) {
            Utils.snack(this, "没有更多数据了~~");
            return;
        }
        String pageNumTemp = viewModel.getMeterQueryResult().getPageNum();
        queryMeterInfo(false, (Integer.valueOf(pageNumTemp) - 1) + "");
    }

    public void nextQuery(View view) {
        if (TextUtils.isEmpty(viewModel.getMeterQueryResult().getPageNum())
                || Integer.valueOf(viewModel.getMeterQueryResult().getPageNum()) >= Integer.valueOf(viewModel.getMeterQueryResult().getRowCount())) {
            Utils.snack(this, "没有更多数据了~~");
            return;
        }
        String pageNumTemp = viewModel.getMeterQueryResult().getPageNum();
        queryMeterInfo(false, (Integer.valueOf(pageNumTemp) + 1) + "");
    }
}
