package com.ruhaly.water.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.GridLayoutManager;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.jude.easyrecyclerview.decoration.SpaceDecoration;
import com.ruhaly.water.R;
import com.ruhaly.water.databinding.ActivityMainBinding;
import com.ruhaly.water.model.Menu;
import com.ruhaly.water.net.response.LoginResponse;
import com.ruhaly.common_lib.base.BaseActivity;
import com.ruhaly.common_lib.utils.JsonUtil;


public class MainActivity extends BaseActivity<ActivityMainBinding> {

    LoginResponse loginResponse;

    Adapter adapter;

    @Override
    public void initTitleFrame() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        String menuStr = getIntent().getStringExtra("MENU");
        loginResponse = JsonUtil.fromJson(menuStr, LoginResponse.class);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        getB().recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new Adapter(getBaseContext());
        gridLayoutManager.setSpanSizeLookup(adapter.obtainGridSpanSizeLookUp(2));
        SpaceDecoration spaceDecoration = new SpaceDecoration(2);
        spaceDecoration.setPaddingEdgeSide(false);
        spaceDecoration.setPaddingStart(false);
        getB().recyclerView.addItemDecoration(spaceDecoration);
        getB().recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(position -> {
//            Utils.snack(MainActivity.this, adapter.getItem(position).getUrl());
            Intent intent = null;
            switch (adapter.getItem(position).getId()) {
                case "1001": {
                    //"快速抄表
                    intent = new Intent(this, FastReadMeterActivity.class);
                    break;
                }
                case "1002": {
                    //"取消抄表
                    intent = new Intent(this, CancelReadMeterActivity.class);
                    break;
                }
                case "1003": {
                    //"已抄查询
                    intent = new Intent(this, HasReadMeterQueryActivity.class);
                    break;
                }
                case "2001": {
                    //"快速收费
                    intent = new Intent(this, FastChargeActivity.class);
                    break;
                }
                case "2002": {
                    //"取消收费
                    intent = new Intent(this, CancelChargeActivity.class);
                    break;
                }
                case "2003": {
                    //"收费查询
                    intent = new Intent(this, ChargeQueryActivity.class);
                    break;
                }


            }
            intent.putExtra("EMPLOYEE_ID", loginResponse.getEmployeeid());
            startActivity(intent);
        });
        adapter.addAll(loginResponse.getMenus());

    }

    class Adapter extends RecyclerArrayAdapter<Menu> {

        public Adapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(parent, R.layout.item_menu);
        }
    }

    class ViewHolder extends BaseViewHolder<Menu> {
        TextView menuName;

        public ViewHolder(ViewGroup parent, @LayoutRes int res) {
            super(parent, res);
            menuName = $(R.id.menu_name);
        }

        @Override
        public void setData(Menu data) {
            menuName.setText(data.getText());
        }
    }
}
