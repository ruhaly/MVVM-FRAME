package com.ruhaly.water.model;

import android.text.TextUtils;

import com.ruhaly.common_lib.model.BaseModel;

/**
 * Created by han_l on 2017-01-18.
 */
public class MeterQueryResult extends BaseModel {
//    userid	用户ID	String
//    usercode	用户编号	String
//    username	用户姓名	String
//    useraddr	用户地址	String
//    lastreading	上次读数	String
//    lastwateramount	上次水量	String
//    lastreadingdate	上次抄表日期	String
//    telephone	座机	String
//    mobilephone	手机	String
//    watertype	用水类型	String
//    meterkind	水表类型	String
//    bookid	册号	String
//    regionid	区域	String
//    rowCount	总条数	String
//    pageSize	每页条数

    String employeeid;
    String userid;
    String usercode;
    String username;
    String useraddr;
    String lastreading = "0";
    String lastwateramount = "0";
    String lastreadingdate;
    String telephone;
    String mobilephone;
    String watertype;
    String meterkind;
    String bookid;
    String regionid;
    String rowCount = "";
    String pageSize;

    String readmonth;

    String currentreading = "0";

    String userwaterinfoid;

    String wateramount;

    String adjustedamount;

    String meterreadingdate;

    String chargedate;
    String parentid;
    String pageNum = "";

    public String getPageNum() {
        return TextUtils.isEmpty(pageNum) ? "1" : pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getChargedate() {
        return chargedate;
    }

    public void setChargedate(String chargedate) {
        this.chargedate = chargedate;
    }

    public String getMeterreadingdate() {
        return meterreadingdate;
    }

    public void setMeterreadingdate(String meterreadingdate) {
        this.meterreadingdate = meterreadingdate;
    }

    public String getAdjustedamount() {
        return adjustedamount;
    }

    public void setAdjustedamount(String adjustedamount) {
        this.adjustedamount = adjustedamount;
    }

    public String getWateramount() {
        return TextUtils.isEmpty(wateramount) ? "0" : wateramount;
    }

    public void setWateramount(String wateramount) {
        this.wateramount = wateramount;
    }

    public String getUserwaterinfoid() {
        return userwaterinfoid;
    }

    public void setUserwaterinfoid(String userwaterinfoid) {
        this.userwaterinfoid = userwaterinfoid;
    }

    public String getCurrentreading() {
        return TextUtils.isEmpty(currentreading) ? "0" : currentreading;
    }

    public void setCurrentreading(String currentreading) {
        this.currentreading = currentreading;
    }

    public String getReadmonth() {
        return readmonth;
    }

    public void setReadmonth(String readmonth) {
        this.readmonth = readmonth;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUseraddr() {
        return useraddr;
    }

    public void setUseraddr(String useraddr) {
        this.useraddr = useraddr;
    }

    public String getLastreading() {
        return TextUtils.isEmpty(lastreading) ? "0" : lastreading;
    }

    public void setLastreading(String lastreading) {
        this.lastreading = lastreading;
    }

    public String getLastwateramount() {
        return TextUtils.isEmpty(lastwateramount) ? "0" : lastwateramount;
    }

    public void setLastwateramount(String lastwateramount) {
        this.lastwateramount = lastwateramount;
    }

    public String getLastreadingdate() {
        return lastreadingdate;
    }

    public void setLastreadingdate(String lastreadingdate) {
        this.lastreadingdate = lastreadingdate;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public String getWatertype() {
        return watertype;
    }

    public void setWatertype(String watertype) {
        this.watertype = watertype;
    }

    public String getMeterkind() {
        return meterkind;
    }

    public void setMeterkind(String meterkind) {
        this.meterkind = meterkind;
    }

    public String getBookid() {
        return bookid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    public String getRegionid() {
        return regionid;
    }

    public void setRegionid(String regionid) {
        this.regionid = regionid;
    }

    public String getRowCount() {
        return TextUtils.isEmpty(rowCount) ? "0" : rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public String getPageSize() {
        return TextUtils.isEmpty(pageSize) ? "0" : pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
