package com.ruhaly.water.model;

/**
 * Created by han_l on 2016/9/21.
 */
public class Account {
    private String isReadNotify;
    private String isSendLocation;
    private String saIsSecretary;
    private String saLoginName;
    private String saLoginPassword;
    private String saTradePassword;
    private String secretaryId;
    private String secretaryName;
    private String secretaryTime;
    private String sid;

    public String getIsReadNotify() {
        return isReadNotify;
    }

    public void setIsReadNotify(String isReadNotify) {
        this.isReadNotify = isReadNotify;
    }

    public String getIsSendLocation() {
        return isSendLocation;
    }

    public void setIsSendLocation(String isSendLocation) {
        this.isSendLocation = isSendLocation;
    }

    public String getSaIsSecretary() {
        return saIsSecretary;
    }

    public void setSaIsSecretary(String saIsSecretary) {
        this.saIsSecretary = saIsSecretary;
    }

    public String getSaLoginName() {
        return saLoginName;
    }

    public void setSaLoginName(String saLoginName) {
        this.saLoginName = saLoginName;
    }

    public String getSaLoginPassword() {
        return saLoginPassword;
    }

    public void setSaLoginPassword(String saLoginPassword) {
        this.saLoginPassword = saLoginPassword;
    }

    public String getSaTradePassword() {
        return saTradePassword;
    }

    public void setSaTradePassword(String saTradePassword) {
        this.saTradePassword = saTradePassword;
    }

    public String getSecretaryId() {
        return secretaryId;
    }

    public void setSecretaryId(String secretaryId) {
        this.secretaryId = secretaryId;
    }

    public String getSecretaryName() {
        return secretaryName;
    }

    public void setSecretaryName(String secretaryName) {
        this.secretaryName = secretaryName;
    }

    public String getSecretaryTime() {
        return secretaryTime;
    }

    public void setSecretaryTime(String secretaryTime) {
        this.secretaryTime = secretaryTime;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @Override
    public String toString() {
        return "Account{" +
                "isReadNotify='" + isReadNotify + '\'' +
                ", isSendLocation='" + isSendLocation + '\'' +
                ", saIsSecretary='" + saIsSecretary + '\'' +
                ", saLoginName='" + saLoginName + '\'' +
                ", saLoginPassword='" + saLoginPassword + '\'' +
                ", saTradePassword='" + saTradePassword + '\'' +
                ", secretaryId='" + secretaryId + '\'' +
                ", secretaryName='" + secretaryName + '\'' +
                ", secretaryTime='" + secretaryTime + '\'' +
                ", sid='" + sid + '\'' +
                '}';
    }
}
