package com.ruhaly.water.model;

import com.ruhaly.common_lib.model.BaseModel;

/**
 * Created by han_l on 2016/9/13.
 */

public class User extends BaseModel {
    private String employeeId;
    private String userName;
    private String phone;
    private String pwd;

    private Menu menu;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
