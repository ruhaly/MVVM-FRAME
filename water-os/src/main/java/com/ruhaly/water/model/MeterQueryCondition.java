package com.ruhaly.water.model;

import com.ruhaly.common_lib.model.BaseModel;

/**
 * Created by han_l on 2017-01-18.
 */
public class MeterQueryCondition extends BaseModel {

//    usercode	用户编号
//    username	用户姓名
//    bookid	册号
//    regionid	区域
//    mobilephone	手机
//    telephone	座机
//    readmonth	抄表月份
//    employeeid	员工号
//    pageNum	页码

    String usercode;
    String username;
    String bookid;
    String regionid;
    String mobilephone;
    String telephone;
    String readmonth;
    String employeeid;
    String pageNum = "1";
    String chargedate;

    public String getChargedate() {
        return chargedate;
    }

    public void setChargedate(String chargedate) {
        this.chargedate = chargedate;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBookid() {
        return bookid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    public String getRegionid() {
        return regionid;
    }

    public void setRegionid(String regionid) {
        this.regionid = regionid;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getReadmonth() {
        return readmonth;
    }

    public void setReadmonth(String readmonth) {
        this.readmonth = readmonth;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }
}
