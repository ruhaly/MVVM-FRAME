package com.ruhaly.water.model;

import com.ruhaly.common_lib.model.BaseModel;

/**
 * Created by han_l on 2017-01-17.
 */
public class Server extends BaseModel {

    private String ip;
    private String port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
