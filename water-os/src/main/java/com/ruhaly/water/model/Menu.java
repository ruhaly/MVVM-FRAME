package com.ruhaly.water.model;

/**
 * Created by han_l on 2017-01-18.
 */
public class Menu {
//    id	菜单ID	String
//    text	菜单名称	String
//    url	请求URL	String

    private String id;
    private String text;
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
