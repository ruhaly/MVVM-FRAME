package com.ruhaly.water;

import android.content.Intent;
import android.os.Bundle;

import com.ruhaly.water.ui.activity.LoginActivity;
import com.ruhaly.common_lib.base.BaseActivity;

import java.util.concurrent.TimeUnit;

import rx.Observable;

/**
 *
 */
public class StartActivity extends BaseActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_start;
    }


    @Override
    public void initView(Bundle savedInstanceState) {
        Observable.timer(2500, TimeUnit.MILLISECONDS).subscribe(aLong -> {
            startActivity(new Intent(getBaseContext(), LoginActivity.class));
            finish();
        });
    }
    @Override
    public void initTitleFrame() {
    }

    @Override
    public void finish() {
        super.finish();
    }
}
