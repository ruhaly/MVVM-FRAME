package com.ruhaly.water.common;

import com.ruhaly.water.model.Server;
import com.ruhaly.common_lib.utils.JsonUtil;
import com.ruhaly.common_lib.utils.SPUtils;

/**
 * Created by han_l on 2017-01-17.
 */
public class Constant {
    public static final String IP_PORT = "IP_PORT";
    public static final String USER_INFO = "USER_INFO";

    //保存server信息
    public static void saveServerInfo(String serverJson) {
        SPUtils.putString(Constant.IP_PORT, serverJson);
    }

    public static String getServerInfo() {
        return SPUtils.getString(IP_PORT);
    }

    public static String getServerUrl() {
        Server server = JsonUtil.fromJson(getServerInfo(), Server.class);
        return "http://".concat(server.getIp()).concat(":").concat(server.getPort()).concat("/chartvs");
    }

    //保存user信息
    public static void saveUserInfo(String userJson) {
        SPUtils.putString(Constant.USER_INFO, userJson);
    }

    public static String getUserInfo() {
        return SPUtils.getString(USER_INFO);
    }

}
