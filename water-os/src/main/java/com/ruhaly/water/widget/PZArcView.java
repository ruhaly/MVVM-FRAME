package com.ruhaly.water.widget;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

public class PZArcView extends View {
    /**
     * 圆环背景画笔
     */
    private Paint mRoundBgPaint;
    /**
     * 圆环画笔
     */
    private Paint mRoundPaint;
    /**
     * 外圆环背景画笔
     */
    private Paint mOutRoundBgPaint;
    /**
     * 圆环画笔
     */
    private Paint mOutRoundPaint;
    /**
     * 圆环画笔
     */
    private Paint mDotPaint;
    /**
     * 刻度画笔
     */
    private Paint mScalePaint;
    /**
     * 未登录外圆画笔
     */
    private Paint mBigcirclePaint;
    /**
     * 未登录内圆画笔
     */
    private Paint mSmallcirclePaint;
    /**
     * 未登录实心圆画笔
     */
    private Paint mSolidPaint;
    /**
     * 刻度文字颜色
     */
    private int mScaleTextColor = 0xff999999;
    /**
     * 刻度文字高度
     */
    private int[] mScaleStrHeights;
    /**
     * 品值画笔
     */
    private Paint mTextPaint;
    /**
     * 品值描述画笔
     */
    private Paint mTextDescPaint;
    /**
     * 未登录品值描述画笔
     */
    private Paint mNoLoginTextDescPaint;
    /**
     * 圆环背景颜色
     */
    private int roundbg_color = 0xffC0C0C0;
    /**
     * 圆环颜色
     */
    private int round_color = 0xffFC6004;
    /**
     * 未登录外圆
     */
    private int bigcircle_color = 0xffF2F1F1;
    /**
     * 未登录内圆
     */
    private int smallcircle_color = 0xffF7F7F7;
    /**
     * 内圆半径
     */
    private int mRadius;
    /**
     * 圆环的宽度
     */
    private float roundWidth = 30;
    /**
     * 未登录圆环的宽度
     */
    private float bigCircleWidth = 35;
    /**
     * 外圆环的宽度
     */
    private float outroundWidth = 10;
    /**
     * 未登录内圆环间距
     */
    private float noLoginSpace = 120;
    /**
     * 刻度值
     */
    public int[] mScale = {0, 3500, 4500, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9999};
    private String[] mScaleStr = {"新手", "3500", "不错", "5500", "优秀", "6500", "爆发", "7500", "极好", "8500", "爆表"};
    /**
     * 圆环背景矩形
     */
    private RectF mRectF = new RectF();
    /**
     * 外圆环背景矩形
     */
    private RectF mOutRectF = new RectF();
    /**
     * 未登录大圆环背景矩形
     */
    private RectF mBigRectF = new RectF();
    /**
     * 未登录小圆环背景矩形
     */
    private RectF mSmallRectF = new RectF();
    /**
     * 未登录实心圆背景矩形
     */
    private RectF mSlolidRectF = new RectF();
    private Rect mScaleStrRect = new Rect();
    /**
     * 圆环开始角度
     */
    private int startAngle = 150;
    /**
     * 品值分角度
     */
    private int mAngle = 0;
    private int mTempAngle = 0;
    /**
     * 圆环最大角度
     */
    public int maxAngle = 240;
    /**
     * 平均角度
     */
    public int avgAngle = maxAngle / (mScale.length - 1);
    /**
     * 内外圆间距
     */
    private int roundSpace = 50;
    /**
     * 圆心X轴坐标
     */
    private int centerX;
    /**
     * 圆心Y轴坐标
     */
    private int centerY;

    private int mMoral = 4500;
    private String mMoralDesc = "哎呦不错哦";
    private String noLoginDesc = "品值分";
    private boolean mLoginState = false;

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
//	    if(ConstantMethod.checkDeviceHasNavigationBar(getContext())){//判断是否有虚拟按键
//	    	setMeasuredDimension(width/6*5,width/36*25);
//	    }else{
        setMeasuredDimension(width, width / 6 * 5);
//	    }
    }

    public PZArcView(Context context) {
        this(context, null);
    }

    public PZArcView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PZArcView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // 获取默认值
        initPoint();
    }

    private void initPoint() {
        mRoundBgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRoundBgPaint.setColor(roundbg_color);
        mRoundBgPaint.setStrokeWidth(roundWidth);/** 渲染画笔宽度 */
        mRoundBgPaint.setStyle(Paint.Style.STROKE);
        mRoundBgPaint.setStrokeCap(Paint.Cap.ROUND);
        mRoundBgPaint.setAntiAlias(true);  //消除锯齿

        mOutRoundBgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mOutRoundBgPaint.setColor(roundbg_color);
        mOutRoundBgPaint.setStrokeWidth(outroundWidth);/** 渲染画笔宽度 */
        mOutRoundBgPaint.setStyle(Paint.Style.STROKE);
        mOutRoundBgPaint.setStrokeCap(Paint.Cap.ROUND);
        mOutRoundBgPaint.setAntiAlias(true);  //消除锯齿

        mRoundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRoundPaint.setColor(round_color);
        mRoundPaint.setStrokeWidth(roundWidth);/** 渲染画笔宽度 */
        mRoundPaint.setStyle(Paint.Style.STROKE);
        mRoundPaint.setStrokeCap(Paint.Cap.ROUND);
        mRoundPaint.setAntiAlias(true);  //消除锯齿

        mOutRoundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mOutRoundPaint.setColor(round_color);
        mOutRoundPaint.setStrokeWidth(outroundWidth);/** 渲染画笔宽度 */
        mOutRoundPaint.setStyle(Paint.Style.STROKE);
        mOutRoundPaint.setStrokeCap(Paint.Cap.ROUND);
        mOutRoundPaint.setAntiAlias(true);  //消除锯齿

        mBigcirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBigcirclePaint.setColor(bigcircle_color);
        mBigcirclePaint.setStrokeWidth(bigCircleWidth);/** 渲染画笔宽度 */
        mBigcirclePaint.setStyle(Paint.Style.STROKE);
        mBigcirclePaint.setStrokeCap(Paint.Cap.ROUND);
        mBigcirclePaint.setAntiAlias(true);  //消除锯齿

        mSmallcirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSmallcirclePaint.setColor(smallcircle_color);
        mSmallcirclePaint.setStrokeWidth(bigCircleWidth);/** 渲染画笔宽度 */
        mSmallcirclePaint.setStyle(Paint.Style.STROKE);
        mSmallcirclePaint.setStrokeCap(Paint.Cap.ROUND);
        mSmallcirclePaint.setAntiAlias(true);  //消除锯齿

        mSolidPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSolidPaint.setColor(Color.WHITE);
        mSolidPaint.setStrokeWidth(2);/** 渲染画笔宽度 */
        mSolidPaint.setStyle(Paint.Style.STROKE);
        mSolidPaint.setStrokeCap(Paint.Cap.ROUND);
        mSolidPaint.setStyle(Paint.Style.FILL);
        mSolidPaint.setAntiAlias(true);  //消除锯齿

        mDotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDotPaint.setStyle(Paint.Style.STROKE);
        mDotPaint.setColor(round_color);
        mDotPaint.setAntiAlias(true);
        mDotPaint.setStyle(Paint.Style.FILL);
        mDotPaint.setStrokeWidth(2);

        mScalePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mScalePaint.setColor(mScaleTextColor);
        mScalePaint.setTextSize(sp2px(10));
        mScalePaint.setTextAlign(Paint.Align.CENTER);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(round_color);
        mTextPaint.setTextSize(sp2px(48));
        mTextPaint.setTextAlign(Paint.Align.CENTER);

        mTextDescPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextDescPaint.setColor(round_color);
        mTextDescPaint.setTextSize(sp2px(16));
        mTextDescPaint.setTextAlign(Paint.Align.CENTER);

        mNoLoginTextDescPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mNoLoginTextDescPaint.setColor(round_color);
        mNoLoginTextDescPaint.setTextSize(sp2px(16));
        mNoLoginTextDescPaint.setTextAlign(Paint.Align.CENTER);

        int length = mScaleStr.length;
        mScaleStrHeights = new int[length];
        for (int i = 0; i < length; i++) {
            mScalePaint.getTextBounds(mScaleStr[i] + "", 0, (mScaleStr[i] + "").length(), mScaleStrRect);
            mScaleStrHeights[i] = mScaleStrRect.height();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mRadius = getMeasuredWidth() / 3;
        centerX = getMeasuredWidth() / 2;
        //centerY = getMeasuredHeight()/2;
        centerY = (int) (mRadius * 1.25);

        //绘制圆环背景
        mRectF.top = centerY - mRadius;
        mRectF.left = mRadius / 2;
        mRectF.right = mRadius * 2 + mRectF.left;
        mRectF.bottom = centerY + mRadius;
        canvas.drawArc(mRectF, startAngle, maxAngle, false, mRoundBgPaint);
        //绘制外圆环背景
        mOutRectF.top = mRectF.top - roundSpace;
        mOutRectF.left = mRectF.left - roundSpace;
        mOutRectF.right = mRectF.right + roundSpace;
        mOutRectF.bottom = mRectF.bottom + roundSpace;
        canvas.drawArc(mOutRectF, startAngle, maxAngle, false, mOutRoundBgPaint);
        if (mAngle != 0) {
            //绘制圆环
            canvas.drawArc(mRectF, startAngle, mAngle, false, mRoundPaint);
            //绘制外圆环
            canvas.drawArc(mOutRectF, startAngle, mAngle, false, mOutRoundPaint);
        }
        //绘制圆点
        canvas.save();
        canvas.translate(centerX, centerY);
        int dotx = (int) (Math.cos(Math.PI * (startAngle + mAngle) / 180) * (mRadius + roundSpace));
        int doty = (int) (Math.sin(Math.PI * (startAngle + mAngle) / 180) * (mRadius + roundSpace));
        canvas.translate(dotx, doty);
        canvas.drawCircle(0, 0, 10, mDotPaint);
        canvas.restore();

        //绘制刻度
        for (int i = 0; i < mScaleStr.length; i++) {
            canvas.save();
            canvas.translate(centerX, centerY);
            canvas.rotate((float) (240 + 24.05 * i));//0.05 误差
            canvas.translate(0, -mRadius + 40);
            canvas.drawText(mScaleStr[i], 0, mScaleStrHeights[i], mScalePaint);
            canvas.restore();
        }

        if (mLoginState) {//已登录

            //绘制品值
            //canvas.drawText(mMoral+"",centerX,centerY, mTextPaint);
            if (mAngle >= mTempAngle) {
                canvas.drawText(mMoral + "", centerX, centerY, mTextPaint);
            } else {
                canvas.drawText((mMoral / mTempAngle * mAngle) + "", centerX, centerY, mTextPaint);
            }

            //绘制描述
            canvas.drawText(mMoralDesc, centerX, centerY + 100, mTextDescPaint);
        } else {//未登陆
            //绘制外圆环
            mBigRectF.top = mRectF.top + noLoginSpace;
            mBigRectF.left = mRectF.left + noLoginSpace;
            mBigRectF.right = mRectF.right - noLoginSpace;
            mBigRectF.bottom = mRectF.bottom - noLoginSpace;
            canvas.drawArc(mBigRectF, 0, 360, false, mBigcirclePaint);
            //绘制内圆环
            mSmallRectF.top = mRectF.top + (noLoginSpace + bigCircleWidth);
            mSmallRectF.left = mRectF.left + (noLoginSpace + bigCircleWidth);
            mSmallRectF.right = mRectF.right - (noLoginSpace + bigCircleWidth);
            mSmallRectF.bottom = mRectF.bottom - (noLoginSpace + bigCircleWidth);
            canvas.drawArc(mSmallRectF, 0, 360, false, mSmallcirclePaint);
            //绘制中心白圆
            canvas.save();
            canvas.translate(centerX, centerY);
            canvas.drawCircle(0, 0, (mSmallRectF.right - mSmallRectF.left) / 2 - bigCircleWidth / 2, mSolidPaint);
            canvas.restore();

			/*BitmapFactory.Options opts = new BitmapFactory.Options();  
	        opts.inSampleSize = 1;    
	        opts.inJustDecodeBounds = false;  
	        Bitmap bitmap =BitmapFactory.decodeResource(getResources(), R.drawable.icon_moral_lock, opts);  
	        int width=opts.outWidth;  
	        int height=opts.outHeight; 
			canvas.drawBitmap(bitmap, centerX-width/2, centerY-height/4, null); 
			canvas.drawText(noLoginDesc,centerX,centerY-height/2, mNoLoginTextDescPaint);*/
        }


    }

    public void setMoral(int moral) {
        this.mMoral = moral;

        //根据品值分计算角度
        for (int i = 0; i < mScale.length; i++) {
            if (mMoral <= mScale[i]) {
                if (i == 0) {
                    mAngle = 0;
                    break;
                } else {
                    // (5000 - 4500）/(5500 -4500) * 24 + 24* 2
                    mAngle = (mMoral - mScale[i - 1]) / (mScale[i] - mScale[i - 1]) * avgAngle + (avgAngle * (i - 1));
                }
            } else {
                if (i == mScale.length - 1) {
                    mAngle = maxAngle;
                }
            }
        }
        postInvalidate();
    }

    public void setLoginState(boolean loginState) {
        this.mLoginState = loginState;
    }

    public void setAngle(int angle) {
        this.mAngle = angle;
        postInvalidate();
    }

    public void setTempAngle(int angle) {
        this.mTempAngle = angle;
    }

    public void setData(int moral, boolean loginState, int tempAngle) {
        this.mMoral = moral;
        this.mLoginState = loginState;
        this.mTempAngle = tempAngle;
    }

    public void setMoralDesc(String moralDesc) {
        this.mMoralDesc = moralDesc;
    }


    public int sp2px(int sp) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp,
                getResources().getDisplayMetrics()) + 0.5f);
    }

    public int dp2px(int dp) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, getResources().getDisplayMetrics()) + 0.5f);
    }
}