package com.ruhaly.water.viewmodel;

import com.ruhaly.water.model.User;
import com.ruhaly.water.net.response.LoginResponse;
import com.ruhaly.common_lib.base.BaseViewModel;


public class LoginViewModel extends BaseViewModel {

    public User user = new User();


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if (user == null) {
            user = new User();
        }
        this.user = user;
    }

    public LoginResponse loginResponse = new LoginResponse();

}
