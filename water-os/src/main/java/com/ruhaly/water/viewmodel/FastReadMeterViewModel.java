package com.ruhaly.water.viewmodel;

import com.ruhaly.water.model.MeterQueryCondition;
import com.ruhaly.water.model.MeterQueryResult;
import com.ruhaly.common_lib.base.BaseViewModel;

public class FastReadMeterViewModel extends BaseViewModel implements Cloneable{

    MeterQueryCondition meterQueryCondition;
    MeterQueryCondition meterQueryConditionMore;
    MeterQueryResult meterQueryResult;

    String rowCount = "0";

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public MeterQueryCondition getMeterQueryConditionMore() {
        return meterQueryConditionMore;
    }

    public void setMeterQueryConditionMore(MeterQueryCondition meterQueryConditionMore) {
        this.meterQueryConditionMore = meterQueryConditionMore;
    }

    public MeterQueryResult getMeterQueryResult() {
        if (meterQueryResult == null) {
            meterQueryResult = new MeterQueryResult();
        }
        return meterQueryResult;
    }

    public void setMeterQueryResult(MeterQueryResult meterQueryResult) {
        if (meterQueryResult == null) {
            meterQueryResult = new MeterQueryResult();
        }
        this.meterQueryResult = meterQueryResult;
    }

    public MeterQueryCondition getMeterQueryCondition() {
        if (meterQueryCondition == null) {
            meterQueryCondition = new MeterQueryCondition();
        }
        return meterQueryCondition;
    }

    public void setMeterQueryCondition(MeterQueryCondition meterQueryCondition) {
        if (meterQueryCondition == null) {
            meterQueryCondition = new MeterQueryCondition();
        }
        this.meterQueryCondition = meterQueryCondition;
    }

    public FastReadMeterViewModel() {
        super();
    }
}
