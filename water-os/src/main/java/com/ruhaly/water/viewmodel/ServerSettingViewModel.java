package com.ruhaly.water.viewmodel;

import com.ruhaly.water.model.Server;
import com.ruhaly.common_lib.base.BaseViewModel;

public class ServerSettingViewModel extends BaseViewModel {


    public Server server = new Server();


    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        if (null == server) {
            server = new Server();
        }
        this.server = server;
    }

    public ServerSettingViewModel() {
        super();
    }
}
