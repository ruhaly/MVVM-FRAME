package com.ruhaly.water.manager;

import com.ruhaly.common_lib.net.response.BaseResponse;
import com.ruhaly.common_lib.utils.RxSchedulers;
import com.ruhaly.water.model.MeterQueryCondition;
import com.ruhaly.water.model.MeterQueryResult;
import com.ruhaly.water.model.User;
import com.ruhaly.water.net.ApiService;
import com.ruhaly.water.net.response.LoginResponse;
import com.ruhaly.water.net.response.MeterQueryChargeResponse;
import com.ruhaly.water.net.response.MeterQueryResponse;

import rx.Observable;

/**
 * Created by han_l on 2016/9/21.
 */
public class LogicManager {
    private static LogicManager instance;

    public static LogicManager getInstance() {
        LogicManager temp = instance;
        if (instance == null) {
            synchronized (LogicManager.class) {
                temp = instance;

                if (instance == null) {
                    temp = new LogicManager();
                    instance = temp;
                }
            }
        }
        return temp;
    }

    private LogicManager() {
    }

    public Observable<BaseResponse<LoginResponse>> login(User user) {
        return ApiService.login(user).compose(RxSchedulers.io_main());
    }

    public Observable<BaseResponse<MeterQueryResponse>> meterQuery(MeterQueryCondition meterQueryCondition) {
        return ApiService.meterQuery(meterQueryCondition).compose(RxSchedulers.io_main());
    }
    public Observable<BaseResponse<MeterQueryResponse>> meterCancelQuery(MeterQueryCondition meterQueryCondition) {
        return ApiService.meterCancelQuery(meterQueryCondition).compose(RxSchedulers.io_main());
    }

    public Observable<BaseResponse<String>> saveMeterInfo(MeterQueryResult meterQueryResult) {
        return ApiService.saveMeterInfo(meterQueryResult).compose(RxSchedulers.io_main());
    }

    public Observable<BaseResponse<String>> fastCharge(MeterQueryResult meterQueryResult) {
        return ApiService.fastCharge(meterQueryResult).compose(RxSchedulers.io_main());
    }

    public Observable<BaseResponse<MeterQueryChargeResponse>> fastChargeQuery(MeterQueryCondition meterQueryCondition) {
        return ApiService.fastChargeQuery(meterQueryCondition).compose(RxSchedulers.io_main());
    }
    public Observable<BaseResponse<MeterQueryChargeResponse>> chargeCancelQuery(MeterQueryCondition meterQueryCondition) {
        return ApiService.chargeCancelQuery(meterQueryCondition).compose(RxSchedulers.io_main());
    }

    public Observable<BaseResponse<String>> chargeCancel(MeterQueryResult meterQueryResult) {
        return ApiService.chargeCancel(meterQueryResult).compose(RxSchedulers.io_main());
    }

    public Observable<BaseResponse<String>> saveCancelMeterInfo(MeterQueryResult meterQueryResult) {
        return ApiService.saveCancelMeterInfo(meterQueryResult).compose(RxSchedulers.io_main());
    }


    public Observable<BaseResponse<MeterQueryChargeResponse>> hasReadMeterQuery(MeterQueryCondition meterQueryCondition) {
        return ApiService.hasReadMeterQuery(meterQueryCondition).compose(RxSchedulers.io_main());
    }

    public Observable<BaseResponse<MeterQueryChargeResponse>> hasChargeQuery(MeterQueryCondition meterQueryCondition) {
        return ApiService.hasChargeQuery(meterQueryCondition).compose(RxSchedulers.io_main());
    }

    public Observable<String> logout() {
        return ApiService.logout().compose(RxSchedulers.io_main());
    }
}
