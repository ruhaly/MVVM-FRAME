# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\develop\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
 #okhttputils
    -dontwarn com.lzy.okhttputils.**
    -keep class com.lzy.okhttputils.**{*;}

    #okhttpserver
    -dontwarn com.lzy.okhttpserver.**
    -keep class com.lzy.okhttpserver.**{*;}

    #okhttp
    -dontwarn okhttp3.**
    -keep class okhttp3.**{*;}

    #okio
    -dontwarn okio.**
    -keep class okio.**{*;}

    -keep public class * implements com.bumptech.glide.module.GlideModule
    -keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
      **[] $VALUES;
      public *;
    }

    # for DexGuard only
    -keepresourcexmlelements manifest/application/meta-data@value=

    -keep class com.blankj.utilcode.** { *; }
    -keepclassmembers class com.blankj.utilcode.** { *; }
    -dontwarn com.blankj.utilcode.**