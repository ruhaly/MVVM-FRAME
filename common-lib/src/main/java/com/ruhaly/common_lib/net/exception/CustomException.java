package com.ruhaly.common_lib.net.exception;

/**
 * Created by han_l on 2016/9/23.
 */
public class CustomException extends RuntimeException {

    public String code;
    public String message;

    public CustomException(Throwable throwable, String code, String message) {
        super(throwable);
        this.code = code;
        this.message = message;
    }

    public CustomException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public CustomException(String detailMessage, String code, String message) {
        super(detailMessage);
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
