package com.ruhaly.common_lib.net.response;


import android.databinding.BaseObservable;

public class BaseResponse<T> extends BaseObservable {

    public String status;

    public String message;

    T data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
