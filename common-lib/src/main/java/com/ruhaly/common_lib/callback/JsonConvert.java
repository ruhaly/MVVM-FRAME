package com.ruhaly.common_lib.callback;

import com.google.gson.stream.JsonReader;
import com.lzy.okgo.convert.Converter;
import com.ruhaly.common_lib.net.response.BaseResponse;
import com.ruhaly.common_lib.net.response.SimpleResponse;
import com.ruhaly.common_lib.utils.JsonUtil;
import com.ruhaly.common_lib.utils.L;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.Response;

/**
 * ================================================
 * 作    者：jeasonlzy（廖子尧）Github地址：https://github.com/jeasonlzy0216
 * 版    本：1.0
 * 创建日期：16/9/11
 * 描    述：
 * 修订历史：
 * ================================================
 */
public class JsonConvert<T> implements Converter<T> {

    private Type type;

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public T convertSuccess(Response response) throws Exception {

        int netCode = response.code();
        if (200 == netCode) {
            JsonReader jsonReader = new JsonReader(response.body().charStream());

            if (type == null) {
                //以下代码是通过泛型解析实际参数,泛型必须传
                Type genType = getClass().getGenericSuperclass();
                Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
                type = params[0];
            }
            if (!(type instanceof ParameterizedType)) throw new IllegalStateException("没有填写泛型参数");
            Type rawType = ((ParameterizedType) type).getRawType();

            //无数据类型
            if (rawType == Void.class) {
                SimpleResponse baseWbgResponse = JsonUtil.fromJson(jsonReader, SimpleResponse.class);
                //noinspection unchecked
                return (T) baseWbgResponse.toBaseResponse();
            }

            //有数据类型
            if (rawType == BaseResponse.class) {
                L.e(jsonReader.toString());
                BaseResponse baseResponse = JsonUtil.fromJson(jsonReader, type);
                int code = Integer.valueOf(baseResponse.getStatus());
                if (code == 200) {
                    //noinspection unchecked
                    return (T) baseResponse;
                } else if (code == 104) {
                    //比如：用户授权信息无效，在此实现相应的逻辑，弹出对话或者跳转到其他页面等,该抛出错误，会在onError中回调。
                    throw new IllegalStateException("用户授权信息无效");
                } else if (code == 105) {
                    //比如：用户收取信息已过期，在此实现相应的逻辑，弹出对话或者跳转到其他页面等,该抛出错误，会在onError中回调。
                    throw new IllegalStateException("用户收取信息已过期");
                } else if (code == 106) {
                    //比如：用户账户被禁用，在此实现相应的逻辑，弹出对话或者跳转到其他页面等,该抛出错误，会在onError中回调。
                    throw new IllegalStateException("用户账户被禁用");
                } else if (code == 300) {
                    //比如：其他乱七八糟的等，在此实现相应的逻辑，弹出对话或者跳转到其他页面等,该抛出错误，会在onError中回调。
                    throw new IllegalStateException("其他乱七八糟的等");
                } else if (code == 5000) {
                    //比如：其他乱七八糟的等，在此实现相应的逻辑，弹出对话或者跳转到其他页面等,该抛出错误，会在onError中回调。
                    throw new IllegalStateException(baseResponse.message);
                } else {
                    throw new IllegalStateException("错误代码：" + code + "，错误信息：" + baseResponse.message);
                }
            }
            throw new IllegalStateException("基类错误无法解析!");
        } else if (404 == netCode) {
            throw new IllegalStateException("请求路径错误" + netCode);
        } else {
            throw new IllegalStateException("服务器异常" + netCode);
        }


    }
}