package com.ruhaly.common_lib.utils;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class JsonUtil {
    private static Gson gson = null;

    @SuppressWarnings("unused")
    public static void init(HashMap<Type, Object> typeAdapterHashMap) {
        GsonBuilder gsonBuilder = new GsonBuilder();

        for (Map.Entry<Type, Object> entry : typeAdapterHashMap.entrySet()) {
            gsonBuilder.registerTypeAdapter(entry.getKey(), entry.getValue());
        }

        gson = gsonBuilder.create();
    }

    @SuppressWarnings("unused")
    public static Gson getGson() {
        return gson;
    }

    @SuppressWarnings("unused")
    public static String toJson(Object src) {
        return gson.toJson(src);
    }

    @SuppressWarnings("unchecked")
    public static <T> T toBean(String gsonString, Class<T> cls) {
        T t = null;
        if (gson != null) {
            t = gson.fromJson(gsonString, cls);
        }
        return t;
    }

    @SuppressWarnings("unchecked")
    public static <T> T toBean(String gsonString, Type typeOfT) {
        T t = null;
        if (gson != null) {
            t = gson.fromJson(gsonString, typeOfT);
        }
        return t;
    }

    @SuppressWarnings("unchecked")
    public static <T> T toBean(String gsonString, TypeToken<T> type) {
        T t = null;
        if (gson != null) {
            t = gson.fromJson(gsonString, type.getType());
        }

        return t;
    }

    @SuppressWarnings("unused")
    public static <T> T parseFromFile(Class<T> cls, String filePath)
            throws JsonSyntaxException, FileNotFoundException {
        return gson.fromJson(new BufferedReader(new FileReader(filePath)), cls);
    }

    @SuppressWarnings("unused")
    public static <T> T parseFromString(Class<T> cls, String jsonString) {
        return gson.fromJson(jsonString, cls);
    }

    @SuppressWarnings("unused")
    public static <T> T parseFromString(Type type, String jsonString) {
        return gson.fromJson(jsonString, type);
    }

    @SuppressWarnings("unused")
    public static <T> T parseFromInputStream(Class<T> cls, InputStream inputStream)
            throws JsonSyntaxException, FileNotFoundException {
        return gson.fromJson(new BufferedReader(new InputStreamReader(inputStream)), cls);
    }

    @SuppressWarnings("unused")
    public static <T> T parseFromInputStream(Type typeOfT, InputStream inputStream)
            throws JsonSyntaxException {
        return gson.fromJson(new BufferedReader(new InputStreamReader(inputStream)), typeOfT);
    }

    @SuppressWarnings("unused")
    public static String getFormatJson(Object src) throws JsonSyntaxException {
        return gson.toJson(src);
    }

    @SuppressWarnings("unused")
    public static Map<String, String> jsonToMap(String json) {
        Type stringStringMap = new TypeToken<Map<String, String>>() {
        }.getType();

        return gson.fromJson(json, stringStringMap);
    }

    @SuppressWarnings("unused")
    public static String mapToJson(Map<String, String> map) {
        return gson.toJson(map);
    }

    public static int getInt(String key, String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.optInt(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Double getDouble(String key, String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.optDouble(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0.00;
    }

    public static String getString(String key, String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.optString(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static <T> T fromJson(String json, Class<T> type) throws JsonIOException, JsonSyntaxException {
        return gson.fromJson(json, type);
    }

    public static <T> T fromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }

    public static <T> T fromJson(JsonReader reader, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return gson.fromJson(reader, typeOfT);
    }

    public static <T> T fromJson(Reader json, Class<T> classOfT) throws JsonSyntaxException, JsonIOException {
        return gson.fromJson(json, classOfT);
    }

    public static <T> T fromJson(Reader json, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return gson.fromJson(json, typeOfT);
    }

    public static String toJson(Object src, Type typeOfSrc) {
        return gson.toJson(src, typeOfSrc);
    }
}
