package com.ruhaly.common_lib.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Utils {

    public static void snack(Activity activity, String message) {
        snack(activity, message, Snackbar.LENGTH_SHORT);
    }

    public static void snack(Activity activity, String message, int duration) {
        Snackbar.make(activity.findViewById(android.R.id.content), message, duration).show();
    }

    @SuppressWarnings("unused")
    public static void snack(Activity activity, int resId) {
        snack(activity, activity.getString(resId));
    }

    @SuppressWarnings("unused")
    public static void snack(Activity activity, int resId, int duration) {
        snack(activity, activity.getString(resId), duration);
    }


    @SuppressWarnings("unused")
    public static void hideKeyBoard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isObjectEmpty(Object o) {
        return o == null;
    }

    public static void openAppSetting(Context context) {
        try {
            Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + context.getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(i);
        } catch (Exception e) {
            L.e(e.getMessage());
        }
    }

    public static String getPhoneNum(Context context) {
        //创建电话管理
        TelephonyManager tm = (TelephonyManager)
        //与手机建立连接
         context.getSystemService(Context.TELEPHONY_SERVICE);
        //获取手机号码
        String phoneNo = tm.getLine1Number();
        return phoneNo;
    }
}
