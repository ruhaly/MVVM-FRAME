package com.ruhaly.common_lib.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by han_l on 2016/9/21.
 */
public abstract class BaseViewHolder<T,D extends ViewDataBinding> extends RecyclerView.ViewHolder {

    D binding;

    public  BaseViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    public abstract void bind(@NonNull T data);
}
