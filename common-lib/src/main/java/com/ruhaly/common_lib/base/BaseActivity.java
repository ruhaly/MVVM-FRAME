package com.ruhaly.common_lib.base;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.ruhaly.common_lib.R;
import com.ruhaly.common_lib.utils.L;
import com.ruhaly.common_lib.utils.Utils;
import com.trello.rxlifecycle.LifecycleTransformer;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import static com.blankj.utilcode.utils.BarUtils.getStatusBarHeight;


public abstract class BaseActivity<T extends ViewDataBinding> extends RxAppCompatActivity {

    private T dataBinding;
    public boolean isPaused;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (0 != getLayoutId()) {
            dataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        }
        setStatusBar();
        initTitleFrame();
        initView(savedInstanceState);
    }

    public void setStatusBar() {
        StatusBarUtil.setColor(this, getResources().getColor(R.color.colorPrimary), 255);
    }

    public void initTitleFrame() {
        findViewById(R.id.header_back).setOnClickListener(view -> finish());
    }

    public T getB() {
        return dataBinding;
    }

    public <M> M initVM(Class<M> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    public abstract int getLayoutId();

    public abstract void initView(Bundle savedInstanceState);


    protected void onResume() {
        super.onResume();
        this.isPaused = false;
    }

    protected void onPause() {
        super.onPause();
        this.isPaused = true;
    }

    /*加载等待框*/
    public ProgressDialog progressDialog;

    public void showProgress(boolean isShow) {
        if (isShow) {
            if (null == progressDialog) {
                progressDialog = new ProgressDialog();
            }
            progressDialog.display(getSupportFragmentManager());
        } else {
            if (null != progressDialog) {
                progressDialog.dismiss(getSupportFragmentManager());
                progressDialog = null;
            }
        }
    }

    public void handleThrowable(Throwable e) {
        L.e(e);
        Utils.snack(this, e.getMessage());
    }

    public <T> LifecycleTransformer<T> bindViewLifecycleTransformer(boolean showDialog) {
        showProgress(showDialog);
        return showDialog ? progressDialog.bindToLifecycle() : bindToLifecycle();
    }

    public void back(View view) {
        finish();
    }

    public void setTranslucentForImageViewInFragment(Activity activity, View needOffsetView) {
        StatusBarUtil.setTranslucentForImageViewInFragment(activity, needOffsetView);
        StatusBarUtil.setTranslucentForImageView(activity, 0, null);
    }

    public void setStatusBarViewParams(View view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = getStatusBarHeight(getBaseContext());
        view.setLayoutParams(params);
    }

    public void showToast(final String str) {
        if (isFastDoubleClick()) {
            return;
        }
        Toast.makeText(getBaseContext(), str, Toast.LENGTH_SHORT).show();
    }

    private static long lastClickTime;

    public static boolean isFastDoubleClick() {

        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (timeD > 0 && timeD < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UMShareAPI.get(this).release();
    }

    UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onResult(SHARE_MEDIA share_media) {
            showToast("分享成功");
        }

        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {
            showToast("分享失败" + throwable.getMessage());
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media) {
            showToast("分享取消");
        }
    };

    public void shareSNS(String url, String title, String content, String imgUrl) {//搜索用户
        UMImage image = new UMImage(this, imgUrl);//资源文件
        UMWeb web = new UMWeb(url);
        web.setTitle(title);//标题
        web.setDescription(content);
        web.setThumb(image);  //缩略图

        new ShareAction(this).withText(title)
                .setDisplayList(SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE)
                .withMedia(web)
                .setCallback(umShareListener).open();
    }

}
