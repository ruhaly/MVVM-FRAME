package com.ruhaly.common_lib.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trello.rxlifecycle.components.support.RxFragment;

/**
 * Created by han_l on 2016/9/14.
 */

public abstract class BaseFragment<T extends ViewDataBinding> extends RxFragment {

    private T dataBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        initView(savedInstanceState);
        return dataBinding.getRoot();
    }

    public T getB() {
        return dataBinding;
    }

    public <M> M initVM(Class<M> cls) {
        try {
            return cls.newInstance();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public abstract int getLayoutId();

    public abstract void initView(Bundle savedInstanceState);
}
