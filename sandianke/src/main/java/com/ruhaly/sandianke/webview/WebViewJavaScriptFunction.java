package com.ruhaly.sandianke.webview;

public interface WebViewJavaScriptFunction {

	void onJsFunctionCalled(String tag);
}
