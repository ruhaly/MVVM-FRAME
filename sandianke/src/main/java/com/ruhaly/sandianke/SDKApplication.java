package com.ruhaly.sandianke;

import android.app.ActivityManager;
import android.content.Context;

import com.ruhaly.common_lib.BaseApplication;
import com.ruhaly.common_lib.share.ShareConstant;
import com.ruhaly.common_lib.utils.ActivityStack;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsDownloader;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;


/**
 * Created by dell on 2016/3/16.
 */
public class SDKApplication extends BaseApplication {
    private static SDKApplication ins;

    public static SDKApplication getIns() {
        return ins;
    }

    public static Context getContext() {
        return ins.getApplicationContext();
    }


    public void exitApp() {
        ActivityStack.getIns().popupAllActivity();
        int sdkVersion = android.os.Build.VERSION.SDK_INT;
        if (sdkVersion <= 7) {
            String name = getPackageName();
            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//            manager.restartPackage(name);
        } else {
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        ins = this;
        initShare();
        UMShareAPI.get(this);

        //搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。
        TbsDownloader.needDownload(getApplicationContext(), false);
        QbSdk.initX5Environment(this, new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {

            }

            @Override
            public void onViewInitFinished(boolean b) {

            }
        });
    }

    private void initShare() {
        PlatformConfig.setWeixin(ShareConstant.APP_ID_WX, ShareConstant.APP_SECRET_WX);
    }

    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }
}
