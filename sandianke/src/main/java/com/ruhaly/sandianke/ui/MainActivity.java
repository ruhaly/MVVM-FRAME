package com.ruhaly.sandianke.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.ruhaly.common_lib.base.BaseActivity;
import com.ruhaly.sandianke.R;
import com.ruhaly.sandianke.databinding.ActivityMainBinding;
import com.ruhaly.sandianke.viewmodel.MainViewModel;


public class MainActivity extends BaseActivity<ActivityMainBinding> {

    MainViewModel viewModel;

    @Override
    public void initTitleFrame() {
        super.initTitleFrame();
        ((TextView) findViewById(R.id.header_title)).setText("首页");
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        initVM();
       /*以下控件设置*/
    }

    public void initVM() {
        viewModel = initVM(MainViewModel.class);
        getB().setViewModel(viewModel);
    }

}
