package com.ruhaly.sandianke.data;

import com.ruhaly.sandianke.data.service.ILocation;
import com.ruhaly.sandianke.data.service.impl.LocationImpl;

/**
 * Created by han_l on 2017-05-03.
 * 2017-05-03
 */

public class DataModelFactory {
    private static DataModelFactory ourInstance = new DataModelFactory();

    public static DataModelFactory getInstance() {
        return ourInstance;
    }

    public ILocation getLocationInfo() {
        return new LocationImpl();
    }
}
