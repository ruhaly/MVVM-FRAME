package com.ruhaly.sandianke.data.service;

import android.content.Context;

import com.tencent.map.geolocation.TencentLocation;

import rx.Observable;

/**
 * Created by Administrator on 2015/12/4.
 */
public interface ILocation {

    Observable<TencentLocation> getBdLocation(Context context, int level);
}
